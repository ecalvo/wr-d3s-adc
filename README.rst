White Rabbit RF Distribution - WR_D3S_ADC project
-------------------------------------------------

The aim of the project is to provide a system that is able to distribute
a wide bandwith (several MHz) RF signal from some external source and
distribute it to remote nodes over the white rabbit network.

Please do not confuse with a similar project (WR_D3S) wich distribute a
narrower bandwith (several kHz) RF signal.

The gateware (which can be found in the hdl folder) implements
the WR RF DDS ADC distribution Node.
It was designed to be implemented in a SVEC card (VME carrier card) with two
Fmc cards:

- A Fmc-adc-subsamp-125m14b4ch card which works as WR master node at
  the fmc slot 0
- A Fmc-dac-600m12b1cha-DDS card working as WR slave node
  at the fmc slot 1.

The master node samples an RF signal, calculates its phase, compresses the
resulting information and sends this data through a WR network.
It also time stamps pulse edges received at the fmc-adc
trigger input with 1ns resolution using a serializer.

The slave node receives this information, uncompresses and decodes the phase
information, and uses a DDS method to reproduce the initial RF signal and
trigger pulses.

This gateware uses the OHWR wr-node-core core, which instantiates two LM32
cpus (one for dealing with each fmc-card).

It contains also files which have been automatic generated through the OHWR
Wishbone slave generator tool (wbgen2).

Please refer to the following urls for getting additional information about the
different components:

- `White Rabbit Node Reference Design`__
- `Mock Turtle`__
- `Simple VME FMC Carrier (SVEC)`__
- `FMC ADC subsamp 125M 14b 4cha`__
- `FMC DAC 600M 12b 1cha DDS`__
- `WBGen tool`__

__ WhiteRabbit_
__ MockTurtle_
__ Svec_
__ Adc_
__ Dac_
__ wbgen2_

The sw folder contain the sources and executables running on each LM32 cpu,
and the front-end host.
It also contains scripts for loading the drivers, tools for testing, etc.

Several git submodules have been included into the project repository.

- HDL:

  - hdl/ip_cores/etherbone-core
  - hdl/ip_cores/general-cores
  - hdl/ip_cores/vme64x-core
  - hdl/ip_cores/wr-cores
  - hdl/ip_cores/wr-node-core (TO BE UPDATED! )

  It has been tried not to use them recursively.

- SW:

  - sw/mock-turtle-sw :  which itself contains the submodule (fmc-bus)


Directory structure
---------------------
.. code::

  `-- hdl    : HDL sources for the design, synthesis and simulation of the FPGA bitstream.
  |   `-- ip_cores   : submodules required for the HDL project 
  |   |   `-- etherbone-core
  |   |   `-- general-cores
  |   |   `-- vme64x-core
  |   |   `-- wr-cores
  |   |   `-- wr-node-core
  |   `-- rtl
  |   |   `-- Manifest.py
  |   |   `-- serdes-tdc
  |   |   `-- TrevGen
  |   |   `-- wr_d3s_adc
  |   `-- sim -> ip_cores/wr-cores/sim/
  |   `-- sw   : Required for simulating in the hdl testbench the LM32 cpus behaviour
  |   |   `-- common
  |   |   `-- debug-test
  |   |   `-- include
  |   `-- syn  : ISE project. For the moment only the SVEC platform has been developped.
  |   |   `-- svec
  |   `-- testbench
  |   |   `-- cpu_wrapper
  |   |   `-- include
  |   |   `-- mqueue_host
  |   |   `-- mqueue_remote
  |   |   `-- top
  |   |   `-- top_d3s_adc
  |   |   `-- top_etherbone
  |   |   `-- top_svec
  |   |   `-- wr_d3s_adc
  |   |   `-- wr_d3s_adc_Rdata
  |   |   `-- wr_d3s_core
  |   `-- top
  |       `-- svec
  `-- LICENSE    : License file
  `-- README.rst  : This file
  `-- sw    :  C sources, executables, test and configuration scripts.
      `-- d3s_alias.sh
      `-- include
      `-- install_d3s.sh
      `-- lib
      `-- load_d3s.sh
      `-- mock-turtle-sw  : mock-turtle submodule 
      `-- mtrtl-commands.txt : notes about mock-turtle commands frequently used
      `-- rt       : sources for the LM32 cpus
      |   `-- common
      |   `-- d3s_adc   : sources for cpu0 (master)
      |   `-- d3s_adc_slave  :  sources for cpu1 (slave)
      |   `-- Makefile.specific
      `-- setup_test.sh
      `-- tools   :  sources and executables for the host. 
  



.. _WhiteRabbit: http://www.ohwr.org/projects/white-rabbit/wiki/WRReferenceDesign
.. _MockTurtle: http://www.ohwr.org/projects/wr-node-core/wiki
.. _Svec: http://www.ohwr.org/projects/svec/wiki
.. _Adc: http://www.ohwr.org/projects/fmc-adc-subsamp125m14b4cha/wiki
.. _Dac: http://www.ohwr.org/projects/fmc-dac-600m-12b-1cha-dds/wiki
.. _wbgen2: http://www.ohwr.org/projects/wishbone-gen/wiki
