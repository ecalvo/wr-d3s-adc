#!/bin/bash

wbgen2 -V d3s_acq_buffer_wb.vhd -H record -p d3s_acq_buffer_wbgen2_pkg.vhd -K d3s_acq_buffer_wb.vh -s defines -C d3s_acq_buffer_wb.h -D d3s_acq_buffer_wb.html d3s_acq_buffer_wb.wb 

echo ""
echo "Moving WB generated files to the following locations..."
echo ""

mv -v d3s_acq_buffer_wb.html ../doc/.
mv -v d3s_acq_buffer_wb.vhd ../.
mv -v d3s_acq_buffer_wbgen2_pkg.vhd ../.
mv -v d3s_acq_buffer_wb.vh ../../../testbench/include/.
cp -v d3s_acq_buffer_wb.h ../../../sw/include/hw/.
mv -v d3s_acq_buffer_wb.h ../../../../sw/rt/common/hw/.
