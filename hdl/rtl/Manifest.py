modules = { 
"local" : [ "wr_d3s_adc", "serdes-tdc", "TrevGen", 
            "../ip_cores/general-cores", 
            "../ip_cores/wr-cores", 
            "../ip_cores/etherbone-core", 
            "../ip_cores/wr-node-core/hdl/rtl/wrnc"
]  };
