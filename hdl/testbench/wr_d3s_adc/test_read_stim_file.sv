`timescale 1ns/1ps
   
//// Attempt to read the file with real aquired data
module read_file
  (
   input clk_wr,
   input enc_started,
   output [13:0] sine2
   );
     
     integer data_file, scan_file;
     logic  signed[13:0] captdata;
     integer sample_n;
     
     initial begin
         data_file = fopen("adc.dat", "r"); //place here file with acq.samples
         if (data_file == 0) begin
             $display("ERROR : CAN NOT OPEN THE FILE");
             $finish;
         end
     end
     
     always @(posedge clk_wr) begin
         if (enc_started == 1) begin
             if (!$feof(data_file)) begin 
                scan_file = $fscanf(data_file, "%d %d\n", sample_n, sine2);
                $display("Data read from file: %d\n",sine2);
             end else begin 
                $fclose(data_file);
                $finish;
             end
         end
     end
     
endmodule

module main;
  
  parameter int g_clock_freq = 125000;
  
  reg rst_n = 0;
  reg clk_wr =0, clk_wr_2x = 0;
  reg clk_sys =0;
  reg clk_adc=0;
  reg enc_started=0;
  reg [39:0]  tm_tai = 100;
  reg [31:0]  tm_nsec = 0;
  
  // =-=-=-=-=-=-=-=- CLOCK GENERATION -==-=-=-=-=-=-=-=-=-=-
   always #1ns clk_adc <= ~clk_adc; // 500 MHz ADC/DAC clock
   
   always@(posedge clk_adc)
     clk_wr_2x <= ~clk_wr_2x;  // 250 MHz

   always@(posedge clk_wr_2x)
     clk_wr <= ~clk_wr;        // 125 MHz

   always@(posedge clk_wr)
     clk_sys <= ~clk_sys;      // 62.5 MHz

   initial begin
      repeat(20) @(posedge clk_sys);
      rst_n = 1;
   end
   // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   
   // =-=-=-=-=-=-=-=-=-  1GHz nsec counter    =-=-=-=-=-=-=-=
   always@(posedge clk_adc or negedge clk_adc) 
   begin
      if(tm_nsec == (g_clock_freq * 8 - 1)) 
      begin
	        tm_tai <= tm_tai + 1;
	        tm_nsec <= 0;
      end else
	        tm_nsec <= tm_nsec + 1;
   end
   // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
   
   initial begin
     #5us;
     enc_started <= 1;
     #5us;
   end
   
 endmodule
 
 