onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Analog-Step -height 25 -max 1.0 -min -1.0 /main/sine_in
add wave -noupdate -format Analog-Step -height 25 -max 1.0 -min -1.0 /main/sine_in_d0
add wave -noupdate /main/a_rf
add wave -noupdate /main/frev_in
add wave -noupdate /main/clk_rf
add wave -noupdate /main/fbunch_in
add wave -noupdate -radix unsigned /main/fbunch_count
add wave -noupdate /main/clk_rf
add wave -noupdate /main/DUT_S/cmp_TrevGen/bunch_tick_i
add wave -noupdate /main/frev_in
add wave -noupdate /main/tm_tai
add wave -noupdate /main/tm_cyc
add wave -noupdate /main/tm_nsec
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/sys_rst_n_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/clk_sys_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/clk_125m_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/serdes_clk_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/serdes_strobe_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/stdc_input_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/timestamp_8th
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/timestamp_8th_reg
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/detect
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/got_a_pulse
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/cycles_i
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/cycles_reg
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/tai_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/tai_reg
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/strobe_o
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/ts_nsec_o
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/ts_tai_o
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/polarity
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_clear
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_full
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_we
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_di
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_empty
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_re
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/fifo_do
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/regs_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/regs_o
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_addr_i
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/wb_data_i
add wave -noupdate -group STDC -radix hexadecimal /main/DUT_M/cmp_stdc/wb_data_o
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_cyc_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_sel_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_stb_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_we_i
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_ack_o
add wave -noupdate -group STDC /main/DUT_M/cmp_stdc/wb_stall_o
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/rst_n_sys_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/clk_sys_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/clk_125m_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/enable_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/tm_time_valid_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/tm_tai_i
add wave -noupdate -group TrevGen -radix hexadecimal /main/DUT_S/cmp_TrevGen/tm_cycles_i
add wave -noupdate -group TrevGen /main/DUT_M/cmp_stdc/stdc_input_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/bunch_tick_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/Trev_tick_o
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_adr_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_dat_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_dat_o
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_cyc_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_sel_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_stb_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_we_i
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_ack_o
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/wb_stall_o
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/s_state
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/s_BclkGate
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/s_Trev_tick
add wave -noupdate -group TrevGen /main/DUT_S/cmp_TrevGen/s_phase
add wave -noupdate -group TrevGen -radix unsigned -childformat {{/main/DUT_S/cmp_TrevGen/s_WRcycTarget(27) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(26) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(25) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(24) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(23) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(22) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(21) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(20) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(19) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(18) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(17) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(16) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(15) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(14) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(13) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(12) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(11) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(10) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(9) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(8) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(7) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(6) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(5) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(4) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(3) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(2) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(1) -radix hexadecimal} {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(0) -radix hexadecimal}} -subitemconfig {/main/DUT_S/cmp_TrevGen/s_WRcycTarget(27) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(26) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(25) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(24) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(23) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(22) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(21) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(20) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(19) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(18) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(17) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(16) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(15) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(14) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(13) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(12) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(11) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(10) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(9) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(8) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(7) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(6) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(5) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(4) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(3) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(2) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(1) {-height 16 -radix hexadecimal} /main/DUT_S/cmp_TrevGen/s_WRcycTarget(0) {-height 16 -radix hexadecimal}} /main/DUT_S/cmp_TrevGen/s_WRcycTarget
add wave -noupdate -group TrevGen -expand /main/DUT_S/cmp_TrevGen/s_regs_i
add wave -noupdate -group TrevGen -childformat {{/main/DUT_S/cmp_TrevGen/s_regs_o.trevgen_rm_next_tick_o -radix hexadecimal}} -expand -subitemconfig {/main/DUT_S/cmp_TrevGen/s_regs_o.trevgen_rm_next_tick_o {-height 16 -radix hexadecimal}} /main/DUT_S/cmp_TrevGen/s_regs_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/clk_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rst_n_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_data_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_dphase_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/raw_phase_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/raw_hp_data_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_max_run_len_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_max_error_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_min_error_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_transient_threshold_phase_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_transient_threshold_count_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/r_record_count_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/fifo_en_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/fifo_full_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/fifo_lost_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/fifo_payload_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/fifo_we_o
add wave -noupdate -group Enc -radix decimal /main/DUT_M/U_Phase_Enc/tm_cycles_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/cnt_fixed_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/lt_cnt_rl_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/st_cnt_rl_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/cnt_ts_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_state_o
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_i
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_i_pre
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_q
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_q_pre
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_phase
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/dummy
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_dphase
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_dphase_d0
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_ddphase
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_ddphase_abs
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_phase_d0
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/flag
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_lt_predelay
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_st_predelay
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_lt_out
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_st_out
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_st
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_lt
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_st_d
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/avg_lt_d
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_phase
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_detect_phase
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_theshold_hit
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_threshold_cnt
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_found
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_count
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_bound_st_lo
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_bound_st_hi
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_bound_lt_lo
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_bound_lt_hi
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_integ
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_integ_next
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_phase_ext
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/transient_integ
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_length
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_state
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/rl_cycles_start
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_lt_bound
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_st_bound
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_hp_out
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/adc_data_reg
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c1
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c2
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c1_d
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c2_d
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c_out
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/c2_pending
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/ts_report_cnt
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_st_lo
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_st_hi
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_lt_lo
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/err_lt_hi
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/cnt_fix
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/lt_cnt_rl
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/st_cnt_rl
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/cnt_ts
add wave -noupdate -group Enc /main/DUT_M/U_Phase_Enc/ddphase_threshold_hit
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/clk_wr_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/rst_n_wr_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/r_enable_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/r_delay_coarse_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/tm_time_valid_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/tm_tai_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/tm_cycles_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/fifo_payload_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/fifo_empty_i
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/fifo_rd_o
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/phase_o
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/phase_valid_o
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_valid_comb
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_valid
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_phase
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_is_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_tstamp
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s2_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_ts_match
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_ts_miss
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_valid
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_phase
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_dphase
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_count
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s3_state
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s1_phase
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s1_is_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s1_valid
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s1_tstamp
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/s1_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/fifo_rd
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/fifo_rd_d
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/stall
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/got_fixup
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/tm_cycles_adj0
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/tm_cycles_adj1
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_phase
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_is_rl
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_tstamp
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_empty
add wave -noupdate -group Dec /main/DUT_S/U_Phase_Dec/ofifo_rd
add wave -noupdate -expand -group Output -format Analog-Step -height 84 -max 16128.000000000002 -min 256.0 -radix unsigned /main/DUT_S/dac_p_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 16} {471266701 ps} 0}
configure wave -namecolwidth 273
configure wave -valuecolwidth 156
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {470217761 ps} {474334241 ps}
