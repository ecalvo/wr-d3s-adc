files = [ "synthesis_descriptor.vhd", "svec_top.vhd", "svec_top.ucf" ]

fetchto = "../../ip_cores"

modules = {
    "local" : [	"../../../rtl", 
		"../node_template", 
		"../../../ip_cores/vme64x-core",
        "../../../ip_cores/etherbone-core/",
        "../../../ip_cores/general-cores/",
        "../../../ip_cores/wr-cores/", 
        "../../../ip_cores/wr-node-core/" ],
    }
