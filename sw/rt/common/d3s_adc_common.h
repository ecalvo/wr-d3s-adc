#ifndef __D3S_ADC_COMMON_H__
#define __D3S_ADC_COMMON_H__

#include <libmockturtle-rt.h>
#include "mockturtle-rt-common.h"
#include "mockturtle-rt-mqueue.h"
#include "d3s_host_rt_common.h"
#include "loop-queue.h"

// FW Core address offsets
#define BASE_D3S_ADC_MASTER 0             // Base address of D3S_ADC_MASTER core
#define BASE_D3S_ADC_SLAVE 0x2000  // Base address of D3S_ADC_SLAVE core
                                   // Both cores are implemented in the same SVEC card
#define MASTER 1
#define SLAVE 0

// REMOTE SLOTs
#define D3S_REMOTE_OUT 0
#define D3S_REMOTE_IN 0

// D3S messages types
#define D3S_MSG_RF  0
#define D3S_MSG_TREV_UPDATE 1

// Transmission coalescece parameters
#define D3S_RF_MSG_MAX_COUNT 5     // Max. number of samples per RF_MSG
#define D3SS_DELAY_COARSE 40000    //Slave coarse delay in number of 8ns ticks
//#define MAX_RL_RECORDS_PER_MESSAGE 10  // Max RL_LENGTH_MAX=4000*8ns=32us, 32us*10=320us
#define RECORDS_PER_MESSAGE 32

// D3S RF encoding and compression parameters
#define D3S_COMP_MAX_ERR 3  // Maximum error bound (in degrees) used in the
//#define D3S_ST_COMP_MAX_ERR 6  // compression algorithm
                               // to send the dphase/dt instead of phase
#define D3S_TRANS_THR_PHASE 50
#define D3S_TRANS_THR_COUNT 6
#define RL_LENGTH_MAX 4000      //Maximum length of RL

// Trev TOF compensation parameters
#define REQ_TREV_PRE_WARNING 10 // Minimum pre-warning for next Trev Tick [in WR cycles]
#define TREV_MIN 20000  // SPS Trev~23us -some arbitrary margin [in us]
#define TREV_MAX 25000  // SPS Trev~23us +some arbitrary margin [in us]
#define HN 4620         // SPS Harmonic number
#define PI 3.141592653
#define LATENCY 25000   // [in number of WR_cycles] About 200us (200us/8ns=25000)


// Data struct used in RF messages type
struct RF_payload {
    uint32_t plen;  // length of RF_data
    uint32_t RF_data[RECORDS_PER_MESSAGE];
};

// Data struct used in Trev messages type
struct TREV_payload { // Time stamp of Trev tick event
    uint32_t ts_tai_h;
    uint32_t ts_tai_l;
    uint32_t ts_ns;
};

struct tx_ts {
    uint32_t tx_seconds;
    uint32_t tx_cycles;
};

// Generic message structure used in D3S-ADC application
struct d3s_msg {
    int type;                     // Message type: D3S_MSG_RF / D3S_MSG_TREV_UPDATE
//    int stream_id;
    union {        // Message payload (which will depend on the message type)
        struct RF_payload RF_payload;
        struct TREV_payload TREV_payload;
    };                           // Max size:  (128+1) 32b-words
};

// Generic message structure used in D3S-ADC application
// when streaming is through Etherbone
struct d3s_Eth_msg {
    struct rmq_message_addr hdr;  // defined @ mockturtle-sw/rt/mockturtle-rt-mqueue.h : SIZE=3x32b-words
    struct tx_ts tx_ts;                 // transmission time stamp to analyze latency
    struct d3s_msg msg;           // d3s-adc data payload
};                                // Max size:  (128+1+2+3) 32b-words

extern void gpior_set(uint32_t mask, int value, uint32_t reg);
extern int  gpior_get(uint32_t mask, uint32_t reg);
extern void init_TOF_vars(struct d3s_adc_dev *dev);
extern void init_RL(struct d3s_adc_dev *dev);

#endif
