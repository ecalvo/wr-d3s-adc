/*
 * This work is part of the White Rabbit Node Core project.
 *
 * Copyright (C) 2013-2014 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */


/*.
 * WR Trigger Distribution (WRTD) Firmware.
 *
 * loop-queue.c: Shared Memory-based Loopback queue.
 */

#include "loop-queue.h"

static SMEM int head, tail, count;
static SMEM struct d3s_msg buf[LOOP_QUEUE_SIZE];

void loop_queue_init()
{
	head = tail = count = 0;
	pp_printf("head=%d, tail= %d, count=%d \n", head, tail, count);
}

struct d3s_msg *loop_queue_claim()
{
	if(count == LOOP_QUEUE_SIZE)
		return NULL;

	return &buf[head];
}

void loop_queue_push(int record)
{
	smem_atomic_add(&head, 1);

	if (head == LOOP_QUEUE_SIZE)
		head = 0;

	smem_atomic_add(&count, 1);
}

struct d3s_msg *loop_queue_get()
{
	if(!count)
		return NULL;

	return &buf[tail];
}

void loop_queue_pop()
{
	if(!count)
		return;  // Loop_queu is EMPTY

	smem_atomic_add(&tail, 1);

	if(tail == LOOP_QUEUE_SIZE)
		tail = 0;

	smem_atomic_sub(&count, 1);
}
