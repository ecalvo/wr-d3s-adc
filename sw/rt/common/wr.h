#include <libmockturtle-rt.h>

#include "hw/wr_d3s_adc.h"
#include "hw/wr_d3s_adc_slave.h"
#include "d3s_adc_common.h"


#define WR_CYCLE_MAX_VALUE (125000000 - 1)
#define WR_CYCLE_MAX_VALUE_NS (WR_CYCLE_MAX_VALUE << 3)

extern void wr_update_link(struct d3s_adc_dev *d3s);
extern void wr_init(struct d3s_adc_dev *d3s);

static inline int wr_is_timing_ok(struct d3s_adc_dev *d3s)
{
    return (d3s->wr_state == WR_LINK_SYNCED);
}
