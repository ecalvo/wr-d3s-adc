#include "wr.h"

// WR aux clock disciplining

static int wr_link_up(struct d3s_adc_dev *d3s)
{
    uint32_t tcr_addr, mask;

    if(d3s->mode == D3S_MODE_MASTER)
    {
        tcr_addr = BASE_D3S_ADC_MASTER + D3S_REG_TCR;
        mask = D3S_TCR_WR_LINK;
    }
    else
    {
        tcr_addr = BASE_D3S_ADC_SLAVE + D3SS_REG_TCR;
        mask = D3SS_TCR_WR_LINK;
    }

    return dp_readl ( tcr_addr ) & mask;
}


static int wr_time_locked(struct d3s_adc_dev *d3s)
{
    uint32_t tcr_addr, mask;

    if(d3s->mode == D3S_MODE_MASTER)
    {
        tcr_addr = BASE_D3S_ADC_MASTER + D3S_REG_TCR;
        mask = D3S_TCR_WR_LOCKED;
    }
    else
    {
        tcr_addr = BASE_D3S_ADC_SLAVE + D3SS_REG_TCR;
        mask = D3SS_TCR_WR_LOCKED;
    }

    return dp_readl ( tcr_addr ) & mask;
}

static int wr_time_ready(struct d3s_adc_dev *d3s)
{
    uint32_t tcr_addr, mask;

    if(d3s->mode == D3S_MODE_MASTER)
    {
        tcr_addr = BASE_D3S_ADC_MASTER + D3S_REG_TCR;
        mask = D3S_TCR_WR_TIME_VALID;
    }
    else
    {
        tcr_addr = BASE_D3S_ADC_SLAVE + D3SS_REG_TCR;
        mask = D3SS_TCR_WR_TIME_VALID;
    }

    return dp_readl ( tcr_addr ) & mask;
}

static void wr_enable_lock(struct d3s_adc_dev *d3s, int enable)
{
    uint32_t tcr_addr, mask;

    if(d3s->mode == D3S_MODE_MASTER)
    {
        tcr_addr = BASE_D3S_ADC_MASTER + D3S_REG_TCR;
        mask = D3S_TCR_WR_LOCK_EN;
    }
    else
    {
        tcr_addr = BASE_D3S_ADC_SLAVE + D3SS_REG_TCR;
        mask = D3SS_TCR_WR_LOCK_EN;
    }

    uint32_t gpior = dp_readl( tcr_addr );

    if(enable)
        dp_writel ( gpior | mask, tcr_addr );
    else
        dp_writel ( gpior & ~mask, tcr_addr );

}

void wr_update_link(struct d3s_adc_dev *d3s)
{
    switch(d3s->wr_state)
    {
        case WR_LINK_OFFLINE:
            if ( wr_link_up(d3s) )
            {
                d3s->wr_state = WR_LINK_ONLINE;
                pp_printf("WR link online!");

            }
            break;

        case WR_LINK_ONLINE:
            if (wr_time_ready(d3s))
            {
                d3s->wr_state = WR_LINK_SYNCING;
                pp_printf("WR time ok [lock on]!");

                wr_enable_lock(d3s, 1);
            }
            break;

        case WR_LINK_SYNCING:
            if (wr_time_locked(d3s))
            {
                pp_printf("WR link locked!");
                d3s->wr_state = WR_LINK_SYNCED;
            }
            break;

        case WR_LINK_SYNCED:
            break;
    }

    if( d3s->wr_state != WR_LINK_OFFLINE && !wr_link_up(d3s) )
    {
        d3s->wr_state = WR_LINK_OFFLINE;
        wr_enable_lock(d3s, 0);
    }
}


void wr_init(struct d3s_adc_dev *d3s)
{
    d3s->wr_state = WR_LINK_OFFLINE;
    wr_enable_lock(d3s, 0);
}
