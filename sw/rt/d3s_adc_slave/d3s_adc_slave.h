#ifndef __D3S_ADC_SLAVE_H__
#define __D3S_ADC_SLAVE_H__

#include <libmockturtle-rt.h>

#include "d3s_host_rt_common.h"
#include "d3s_adc_common.h"
#include "wr.h"

// WB interfaces definitions
#include "hw/wr_d3s_adc_slave.h"   //Register definitions of the D3S ADC WB slave
#include "hw/TrevGen_wb_slave.h"   //Register definitions of the TrevGen WB slave

// WB interfaces BASE ADDRESSES
#define BASE_TrevGen 0x100  //Trev Generator

enum d3s_adc_slave_variables {
        D3S_SLAVE_STREAM_ID = 0,
};



extern int ad9516_init();


#endif
