#ifndef __D3S_ADC_MASTER_H__
#define __D3S_ADC_MASTER_H__

#include <libmockturtle-rt.h>

#include "d3s_host_rt_common.h"
#include "d3s_adc_common.h"
#include "wr.h"

// WB interfaces definitions
#include "hw/wr_d3s_adc.h"
#include "hw/stdc_wb_slave.h"
#include "hw/d3s_acq_buffer_wb.h"

// WB interfaces BASE ADDRESSES
#define BASE_ACQ_BUF1  (BASE_D3S_ADC_MASTER + 0x00100)
#define BASE_ACQ_BUF2  (BASE_D3S_ADC_MASTER + 0x00200)
#define BASE_ACQ_BUF3  (BASE_D3S_ADC_MASTER + 0x00300)
#define BASE_D3S_STDC  (BASE_D3S_ADC_MASTER + 0x00400)
#define BASE_ACQ_BUF4  (BASE_D3S_ADC_MASTER + 0x00500)
#define BASE_SI57X     (0x01000)

enum d3s_adc_variables {
    D3S_STREAM_ID = 0,
};

extern void si57x_init(void);
extern void si57x_set_frequency(uint64_t f_out_ref, uint64_t f_out_new);
extern void setup_LTC2175(void);

#endif
