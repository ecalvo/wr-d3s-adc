#include "d3s_adc_common.h"
#include "ltc2175.h"

// ******** Functions for SPI  **********
static void bb_spi_init(int reg_addr)
{
    gpior_set(D3S_GPIOR_SPI_CS_ADC , 1, reg_addr);    //Set to 1 LT2175 ADC chip select
    gpior_set(D3S_GPIOR_SPI_SCK    , 0, reg_addr);    //Set to 0 LT2157 ADC SCK line
    gpior_set(D3S_GPIOR_SPI_MOSI   , 0, reg_addr);    //Set to 0 LT2157 ADC I2C SDO line
    return;
}

// Bit-banged SPI access.
static int bb_spi_txrx(uint32_t cs_mask, int nbits, uint32_t in, uint32_t *out, int reg_addr)
{
    int i;
    uint32_t rv = 0;

    gpior_set(cs_mask, 0, reg_addr);
    delay(10);

    for(i=nbits-1; i>=0 ;i--)
    {
      gpior_set(D3S_GPIOR_SPI_SCK, 0, reg_addr);
      delay(10);
      gpior_set(D3S_GPIOR_SPI_MOSI, in & (1 << i) ? 1 : 0, reg_addr);
      delay(10);
      gpior_set(D3S_GPIOR_SPI_SCK, 1, reg_addr);
      delay(10);
      if (gpior_get(D3S_GPIOR_SPI_MISO, reg_addr ))
          rv |= (1 << i);
    }

    gpior_set(cs_mask, 1, reg_addr);
    delay(10);

    *out = rv;
    return 0;
}


/**
 * setup the ADC data output format to 2's complement
 */
void setup_LTC2175(void)
{
    uint32_t rx;

    bb_spi_init(BASE_D3S_ADC_MASTER + D3S_REG_GPIOR);

    // Force 2's complement data output (register 1, bit 5)
    bb_spi_txrx(D3S_GPIOR_SPI_CS_ADC, 16, (1 << 8) | (1 << 5), &rx, BASE_D3S_ADC_MASTER + D3S_REG_GPIOR);

    pp_printf("setup_LTC2175 done \n");
}
