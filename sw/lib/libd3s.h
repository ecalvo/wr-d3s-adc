#ifndef __LIB3DS_H__
#define __LIB3DS_H__

#include <libmockturtle.h>
#include <d3s_host_rt_common.h>

struct d3s_node;

extern const char *d3s_strerror(int err);
extern int d3s_init();
extern void d3s_exit();
extern int d3s_ping(struct d3s_node *dev);
extern int d3s_version(struct d3s_node *dev, struct trtl_rt_version *version);
extern int d3s_reset(struct d3s_node *dev);
extern int d3s_status(struct d3s_node *dev, struct d3s_adc_dev *status);
extern int d3s_version_is_valid(struct d3s_node *dev);
extern struct d3s_node *d3s_open_by_fmc(uint32_t device_id, enum d3s_mode mode);
extern struct d3s_node *d3s_open_by_lun(int lun, enum d3s_mode mode);
extern void d3s_close(struct d3s_node *dev);

extern int d3s_stream_start(struct d3s_node *dev, uint32_t streamid);
extern int d3s_stream_stop(struct d3s_node *dev);
extern int d3s_stream_is_running(struct d3s_node *dev);
#endif
