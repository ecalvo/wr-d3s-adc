#ifndef __LIB3DS_INTERNAL_H__
#define __LIB3DS_INTERNAL_H__

#include "d3s_host_rt_common.h"
#include "libd3s.h"

struct d3s_desc {
	uint32_t dev_id;
	struct trtl_dev *trtl;
	enum d3s_mode mode;
	struct trtl_hmq *hmq;
};

#endif
