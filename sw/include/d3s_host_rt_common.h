#ifndef __D3S_HOST_RT_H__
#define __D3S_HOST_RT_H__

#include "mockturtle-common.h"

#define D3S_APP_ID 0xDD3F3C02
#define D3S_RT_ID_MASTER 0x4321
#define D3S_RT_ID_SLAVE  0x9876

/* HOST SLOTs (in/out from the real-time point of view) */
#define D3S_IN_CONTROL 0
#define D3S_SLAVE_IN_CONTROL 1

#define D3S_OUT_CONTROL 0
#define D3S_SLAVE_OUT_CONTROL 1

#define D3S_OUT_DEBUG 2
#define D3S_SLAVE_OUT_DEBUG 3

enum d3s_errors {
	ED3S_INVALID_MODE = 90000,
	ED3S_INVALID_IN_APP,
	__ED3S_MAX_ERROR_NUMBER,
};

enum white_rabbit_state {
    WR_LINK_UNKNOWN = 0,
    WR_LINK_OFFLINE,
    WR_LINK_ONLINE,
    WR_LINK_SYNCING,
    WR_LINK_SYNCED,
};

enum d3s_adc_action {
    D3S_ADC_ACTION_START = __RT_ACTION_RECV_STANDARD_NUMBER,
    D3S_ADC_ACTION_STOP,
};

#define D3S_ADC_FLAG_RUNNING (1 << 0)

enum d3s_mode {
	D3S_MODE_MASTER = 0,
	D3S_MODE_SLAVE,
};

struct d3s_stats_slave {
	uint32_t dec_state;
	uint32_t latency;
	uint32_t phase_fifo_overflow;
	uint32_t msg_rf_recv;
	uint32_t msg_tr_recv;
	uint32_t msg_invalid;
};

struct d3s_stats_master {
	uint32_t enc_state;
	uint32_t msg_failure;
	uint32_t msg_rf_sent;
	uint32_t msg_tr_sent;
	uint32_t fifo_overflow;
	uint32_t stdc_fifo_overflow;
	uint32_t fifo_worst_count;
};

struct d3s_adc_dev {
	uint32_t stream_id; /**< identify a particular stream
				 over a shared network */
	uint32_t flags;
    
    uint32_t mode;

	uint32_t stream_enabled;
    uint32_t err_max;
    //uint32_t lt_err_max;
    uint32_t rl_length_max;
    uint32_t trans_thr_phase;
    uint32_t trans_thr_count;

    uint64_t last_Trev_ts_tai;
    uint32_t last_Trev_ts_ns;
    uint32_t last_Trev_period_ns;
    
	union {
		struct d3s_stats_slave slave;
		struct d3s_stats_master master;
	} stats;

    enum white_rabbit_state wr_state;
};

enum d3s_adc_struct {
	D3S_ADC_STRUCT_DEVICE = 0,
};

#endif
