/*
 * This work is part of the White Rabbit Node Core project.
 *
 * Copyright (C) 2013-2015 CERN (www.cern.ch)
 * Author: Eva Calvo <eva.calvo@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */


/*.
 * Time to Digital converter (TDC) based on 1GHz Xilinx serializer.
 *
 * d3s-stdc-test.c: Script for testing 1 ns second STDC time stamping
 */


#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include<time.h>

#include "wr-d3s-common.h"

#ifdef WRNODE_RT
#include "rt-common.h"
#include "rt-d3s.h"

#define dbg_printf pp_printf
#else

// linux env

#define dbg_printf printf

#endif


// WB interfaces
#include "stdc_wb_slave.h"
#include "wr_d3s_adc.h"



// Core base addresses
#define BASE_STDC 0x10400  // 0x10400
#define BASE_D3S  0x10000

uint32_t svec_read(uint32_t addr);
void svec_write(uint32_t data, uint32_t addr);



static void stdc_init(uint32_t edge_detection)
{
    /* Clear the serdes_tdc fifo */
    svec_write(STDC_CTRL_CLR | STDC_CTRL_CLR_OVF, BASE_STDC + STDC_REG_CTRL);
    //dbg_printf("STDC: Fifo cleared: %x \n", svec_read(BASE_STDC + STDC_REG_CTRL));
    //delay(1000);
    
    /* Filter bits sets to 01 to detect only rising edges */
    svec_write(STDC_CTRL_FILTER_W(edge_detection), BASE_STDC + STDC_REG_CTRL);
    //dbg_printf("STDC: Edge filter configured: %x \n", svec_read(BASE_STDC + STDC_REG_CTRL));
    //delay(1000);
 
}

static void read_stdc_data(char *filename, uint8_t edge_detection)
{
    uint32_t p_WRcyc = 0 ;
    uint32_t p_ns_tick = 0 ;
    
    uint32_t count = 0;

    FILE *f = fopen(filename,"a") ;
    
    // Check if there is any event to process in the fifo

    uint32_t stat = svec_read(BASE_STDC + STDC_REG_STATUS);
    
    printf("STDC: stat %x ctrl %x\n", stat, svec_read(BASE_STDC+STDC_REG_CTRL));
    //fprintf(f, "STDC: stat %x ctrl %x\n", stat, svec_read(BASE_STDC+STDC_REG_CTRL));
    int empty = (stat & STDC_STATUS_EMPTY);
    //int full = (stat & STDC_STATUS_OVF);

    while (!empty)
    {
        count ++;

        svec_write(STDC_CTRL_FILTER_W(0) , BASE_STDC + STDC_REG_CTRL);  // disable STDC
                
        uint32_t stdc_stamp = svec_read(BASE_STDC + STDC_REG_TDC_DATA);  // Read data
        
        uint32_t polarity = !! ( stdc_stamp & (1<<31) ) ;

        uint32_t WR_cycle_mask = ((1<<28)-1)<<3 ;
        uint32_t WR_cycle = ( stdc_stamp & WR_cycle_mask ) >>3 ;

        uint32_t WR_cycle_8th_mask = ((1<<3)-1);
        uint32_t WR_cycle_8th = (stdc_stamp & WR_cycle_8th_mask ) ;
                
        if (count>1) //(p_WRcyc != 0 )
        {
             printf("STDC : count=%d, cycle= %d, p_cycle=%d, Trev Period [us]: %.3f \n", \
                    count, WR_cycle, p_WRcyc, \
                    ((WR_cycle - p_WRcyc)*8+(WR_cycle_8th - p_ns_tick))/1e3 ) ;
             //fprintf(f, "STDC : cycle= %d, p_cycle=%d,  Meas frequency [Hz]: %f \n", \
             //        WR_cycle, p_WRcyc, 1e9/((WR_cycle - p_WRcyc)*8+(WR_cycle_8th - p_ns_tick)) ) ;
         }

         p_WRcyc = WR_cycle ;
         p_ns_tick = WR_cycle_8th ;

         // Process next event
         svec_write(STDC_CTRL_NEXT, BASE_STDC + STDC_REG_CTRL);
        
         empty = (svec_read(BASE_STDC + STDC_REG_STATUS) & STDC_STATUS_EMPTY);
   }
        
   svec_write(STDC_CTRL_FILTER_W(edge_detection) , BASE_STDC + STDC_REG_CTRL);  // enable STDC
   fclose(f) ;
   
    
}

int main()
{
    int i;
    char *filename="/tmp/stdc_data.dat";

    printf("ID : %x\n",  svec_read(0));

    svec_write(0, BASE_D3S+D3S_REG_GPIOR);
    usleep(100);    
    
    si57x_test();
    usleep(1000);    
    
    svec_write(D3S_GPIOR_TM_LOCK_EN, BASE_D3S+D3S_REG_GPIOR);
    usleep(100); 
    dbg_printf("WR core initialized.\n");


    // Clear stdc fifo and setups the transitions to detect 
    uint32_t edge_detection = 2; 
                 // = 0 disable edge detection
                 // = 1 enable rising edge detection
                 // = 2 enable falling edge detection
                 // = 3 enable rising and falling edge detection
    stdc_init(edge_detection);
    dbg_printf("STDC initialized.\n");
    
    FILE *f = fopen(filename,"w") ; fclose(f) ;

    for(i=0; i<3000; i++)
    {
         read_stdc_data(filename, edge_detection);
         usleep(1000);
    }
    return(0);
}
