#!/bin/bash

sudo ./d3s-adc-test

prefix=$1

cp /tmp/adc_data.dat ./tmpdata/${prefix}_adc_data.dat
cp /tmp/raw_dphase.dat ./tmpdata/${prefix}_raw_dphase.dat
cp /tmp/raw_phase.dat ./tmpdata/${prefix}_raw_phase.dat
cp /tmp/rl_state.dat ./tmpdata/${prefix}_rl_state.dat
