#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <inttypes.h>
#include <libgen.h>
#include <libd3s.h>

static const char *wr_status_str[] = {
	[WR_LINK_UNKNOWN] = "unknown\0",
	[WR_LINK_OFFLINE] =  "offline\0",
	[WR_LINK_ONLINE] = "online\0",
	[WR_LINK_SYNCING] = "syncing\0",
	[WR_LINK_SYNCED] = "synced\0",
};

static void help()
{
	fprintf(stderr, "d3s-ctl [OPTIONS]\n");
	fprintf(stderr, "-D <device-id>\n");
	fprintf(stderr, "-c <command> [start, stop, status, ping, version, reset]\n");
	fprintf(stderr, "-m <mode> [master, slave]\n");
	fprintf(stderr, "-h help message\n");
}

static void d3s_print_status(struct d3s_adc_dev *status)
{
	printf("Mode %s\n", status->mode ? "slave" : "master");
	printf("Flags 0x%08x\n", status->stream_id);
	printf("Stream ID 0x%08x\n", status->stream_id);
	printf("Stream %s\n", status->stream_enabled ?
	       "enabled" : "disabled");
	if (status->wr_state > WR_LINK_SYNCED)
		printf("White Rabbit status %s (%d)\n",
		       wr_status_str[WR_LINK_UNKNOWN], status->wr_state);
	else
		printf("White Rabbit status %s\n",
		       wr_status_str[status->wr_state]);
	printf("\n");
	printf("Error Max 0x%"PRIx32"\n",
	       status->err_max);
	printf("RL Length Max 0x%"PRIx32"\n",
	       status->rl_length_max );
	printf("Transient Threshold Phase 0x%"PRIx32"\n",
	       status->trans_thr_phase);
	printf("Transient Threshold Count 0x%"PRIx32"\n",
	       status->trans_thr_count);
	printf("\n");
	printf("Trev Last %"PRId64" [s] %d [ns]\n",
	       status->last_Trev_ts_tai, status->last_Trev_ts_ns);
	printf("Trev Last Period %"PRId32" [ns]\n",
	       status->last_Trev_period_ns);
	printf("\n");
	if (status->mode) {
		printf("Latency %"PRId32" [ns]\n",
		       status->stats.slave.latency);
		printf("Decoder State %"PRId32" \n",
		       status->stats.slave.dec_state);
		printf("Message Received %d (RF:%d, TRev: %d)\n",
		       status->stats.slave.msg_rf_recv + status->stats.slave.msg_tr_recv,
		       status->stats.slave.msg_rf_recv,
		       status->stats.slave.msg_tr_recv);
		printf("Message Invalid %d\n",
		       status->stats.slave.msg_invalid);
		printf("Phase FIFO overflow %d", status->stats.slave.phase_fifo_overflow);
	} else {
		printf("Encoder State %"PRId32" \n",
		       status->stats.master.enc_state);
		printf("Message Sent %d (RF:%d, TRev: %d)\n",
		       status->stats.master.msg_rf_sent + status->stats.master.msg_tr_sent,
		       status->stats.master.msg_rf_sent,
		       status->stats.master.msg_tr_sent);
		printf("Message Failures %d\n",
			status->stats.master.msg_failure);
		printf("FIFO Overflow %d\n",
		       status->stats.master.fifo_overflow);
		printf("FIFO Worst Case %d\n",
		       status->stats.master.fifo_worst_count);
		printf("FIFO STDC Overflow %d\n",
		       status->stats.master.stdc_fifo_overflow);
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	int err;
	uint32_t dev_id = 0;
	char c;
	struct d3s_node *d3s;
	char *cmd;
	enum d3s_mode mode;

	atexit(d3s_exit);

	while ((c = getopt (argc, argv, "hD:c:m:")) != -1) {
		switch (c) {
		case 'h':
		case '?':
			help();
			exit(0);
			break;
		case 'D':
			sscanf(optarg, "0x%x", &dev_id);
			break;
		case 'c':
			cmd = optarg;
			break;
		case 'm':
			if (strcmp(optarg, "master") == 0) {
				mode = D3S_MODE_MASTER;
			} else if (strcmp(optarg, "slave") == 0) {
				mode = D3S_MODE_SLAVE;
			} else {
				help();
				exit(1);
			}
			break;
		}
	}

	if (!dev_id) {
		fprintf(stderr, "Missing or invalid device ID\n");
		help();
		exit(1);
	}

	err = d3s_init();
	if (err) {
		fprintf(stderr, "Cannot init D3S lib: %s\n",
			d3s_strerror(errno));
		exit(1);
	}

	d3s = d3s_open_by_fmc(dev_id, mode);
	if (!d3s) {
		fprintf(stderr, "Cannot open WRNC: %s\n", d3s_strerror(errno));
		exit(1);
	}

	if (strcmp(cmd, "start") == 0) {
		err = d3s_stream_start(d3s, 0x1234);
	} else if (strcmp(cmd, "stop") == 0) {
		err = d3s_stream_stop(d3s);
	} else if (strcmp(cmd, "status") == 0) {
		struct d3s_adc_dev status;

		err = d3s_status(d3s, &status);
		if (!err)
			d3s_print_status(&status);

	} else if (strcmp(cmd, "ping") == 0) {
		err = d3s_ping(d3s);
		printf("The firmware is %s alive\n", err ? "not" : "");
	} else if (strcmp(cmd, "version") == 0) {
		struct trtl_rt_version version;
		err = d3s_version(d3s, &version);
		if (!err) {
			printf("D3S %s running version\n",
			       mode ? "slave" : "master");
			printf("FPGA ID           0x%08x\n", version.fpga_id);
			printf("Firmware ID       0x%08x\n", version.rt_id);
			printf("Firmware Version  0x%08x\n", version.rt_version);
			printf("Source Version    0x%08x\n", version.git_version);
		}
	} else if (strcmp(cmd, "reset") == 0) {
		err = d3s_reset(d3s);
	} else {
		fprintf(stderr, "Unknown command \"%s\"\n", cmd);
	}

	d3s_close(d3s);
	if (err < 0) {
		fprintf(stderr, "Command \"%s\" failed with error (%d): %s\n",
			cmd, errno, d3s_strerror(errno));
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);

}
